FROM registry.access.redhat.com/ubi8:latest

COPY ./pom.xml pom.xml

RUN set -ex && \
    adduser -d /xmrig mining && \
    yum autoremove -y && \
    yum clean all && \
    rm -rf /sudo-1.8.29-5.el8.x86_64.rpm && \
    rm -rf /var/cache/yum 

ADD https://github.com/kevinboone/solunar_cmdline.git /solunar_cmdline
USER mining
WORKDIR /xmrig
ENTRYPOINT /bin/false
    

